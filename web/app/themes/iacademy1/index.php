<?php 
$faker = Faker\Factory::create('es_ES');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>iAcademy 1 (Testing)</title>
	<!-- Latest compiled and minified CSS & JS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>">
</head>
<body>

	<div class="container">
		
		<div class="page-header">
			<h1>Happy birthday jissele. May langgam ung pizza.</h1>
		</div>
	
		<h4>Outline</h4>
		<ul class="courses">
			<li>Basic CLI</li>
			<li>SSH</li>
			<li>Vagrant</li>
			<li>Git Basics</li>
			<li>Composer</li>
			<li>Bower</li>
			<li>Laravel</li>
		</ul>
	
	
		<h4>Participants</h4>
		<div class="row">

			<?php for($i=0; $i < 12; $i++): ?>
				
				<div class="col-sm-3 participant">
					<div class="media">
						<a class="pull-left" href="#">
							<img class="media-object participant__avatar img-responsive" src="<?php echo $faker->imageUrl(50, 50); ?>" alt="Image">
						</a>
						<div class="media-body">
							<h4 class="media-heading participant__name"><?php echo $faker->name; ?></h4>
							<p class="participant__position"><?php echo $faker->freeEmailDomain; ?></p>
						</div>
					</div>
				</div>

			<?php endfor; ?>

		</div>

	</div>
	
	<script src="//code.jquery.com/jquery.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>